FROM ubuntu:latest

MAINTAINER Anthony Roscio "@roscioa"

RUN apt-get update && apt-get -y -q install vim python3 git-core python-dev sudo build-essential gcc curl
